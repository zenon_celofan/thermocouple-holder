import bottle, cgi
#import requests

HOST = "10.42.125.99"
PORT = 80

@bottle.get("/")
def message():
    logging_path = bottle.request.query.logging_path_form_field
    if logging_path == "":
        logging_path = "d:\\"



    try:
        log_file = open(logging_path + "test_file.txt", "w+")
        log_file.write("sample text")
        log_file.close()
        can_acess_logging_path = True
    except:
        can_acess_logging_path = False

    return bottle.template('main_page.tpl', logging_path_html_param = logging_path, can_acess_logging_path_html_param = can_acess_logging_path)

@bottle.route('/set')
def set_parameters():
    t1 = bottle.request.query.t1 or "off"
    t2 = bottle.request.query.t2 or "off"
    return "." + t1 + "." + t2

@bottle.route('/get')
def get_parameters():
    forum_id = request.query.id
    page = request.query.page or '1'
    return bottle.template('Forum ID: {{id}} (page {{page}})', id=forum_id, page=page)

@bottle.route("/?logging_path")
def set_logging_path():
    return bottle.template('main_page.tpl', logging_path="c:")





bottle.run(host=HOST, port=PORT)




